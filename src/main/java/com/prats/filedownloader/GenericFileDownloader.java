package com.prats.filedownloader;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.prats.filedownloader.entity.FileDownloadRequest;
import com.prats.filedownloader.entity.FileDownloadResponse;
import com.prats.filedownloader.entity.SubmittedFileDownloadJob;
import com.prats.filedownloader.properties.PropertyReader;
import com.prats.filedownloader.util.UrlParserUtility;

/* Main class providing api for downloading files parallely */
public class GenericFileDownloader {

	public List<FileDownloadResponse> downloadFiles(List<String> urls) {
		// transform given string urls to file download request
		List<FileDownloadRequest> requests = transformUrlsToRequest(urls);

		List<FileDownloadResponse> results = new ArrayList<FileDownloadResponse>();

		if (requests != null && !requests.isEmpty()) {
			Integer threadPoolSize = Integer.valueOf(PropertyReader
					.getPropertyValue(PropertyReader.THREAD_POOL_SIZE_PROPERTY));

			// create a thread pool based on config file property.
			ExecutorService executorService = Executors.newFixedThreadPool(threadPoolSize);
			try {
				// creating a count down latch which will wait for all the
				// threads.
				final CountDownLatch latch = new CountDownLatch(requests.size());

				List<SubmittedFileDownloadJob> submittedJobs = new ArrayList<SubmittedFileDownloadJob>(
						requests.size());

				for (FileDownloadRequest request : requests) {
					// submit all the requests implementing Callable to executor
					// service and keep a track of requests using future
					Future<FileDownloadResponse> future = executorService
							.submit(new ProcessFileDownloadRequest(request, latch));
					submittedJobs.add(new SubmittedFileDownloadJob(future));
				}

				long start = System.currentTimeMillis();
				try {
					System.out.println("Waiting for jobs to be completed.");
					// wait for all the requests to get completed
					latch.await();
				} catch (InterruptedException e1) {
					e1.printStackTrace();
					for (SubmittedFileDownloadJob job : submittedJobs) {
						// cancel the job
						job.getFuture().cancel(true);
					}
				}

				System.out.println("Total Time taken to download all files in sec : "
						+ (System.currentTimeMillis() - start) * 0.001);

				// iterate on all jobs and check for response using future
				for (SubmittedFileDownloadJob job : submittedJobs) {
					try {
						FileDownloadResponse response = job.getFuture().get();
						results.add(response);
					} catch (ExecutionException cause) {
						cause.printStackTrace();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}

			} finally {
				executorService.shutdown();
			}
		}
		return results;
	}

	private List<FileDownloadRequest> transformUrlsToRequest(List<String> urls) {
		List<FileDownloadRequest> requests = new ArrayList<FileDownloadRequest>();
		if (urls != null && !urls.isEmpty()) {
			for (String url : urls) {
				FileDownloadRequest fileDownloadRequest = UrlParserUtility
						.getFileDownloadRequest(url);
				if (fileDownloadRequest != null) {
					requests.add(fileDownloadRequest);
				}
			}
		}
		return requests;
	}

	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		list.add("http://speedtest.ftp.otenet.gr/files/test10Mb.db");
		list.add("https://code.jquery.com/jquery-3.0.0.min.js");
		list.add("ftp://speedtest:speedtest@ftp.otenet.gr/test1Mb.db");
		GenericFileDownloader genericFileDownloaderMain = new GenericFileDownloader();
		List<FileDownloadResponse> results = genericFileDownloaderMain.downloadFiles(list);
		for (FileDownloadResponse f : results) {
			System.out.println(f);
		}
	}

}
