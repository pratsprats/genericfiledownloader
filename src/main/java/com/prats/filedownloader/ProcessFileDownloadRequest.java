package com.prats.filedownloader;

import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

import com.prats.filedownloader.client.FileTransferClient;
import com.prats.filedownloader.client.factory.FileTransferAbstractClientFactory;
import com.prats.filedownloader.client.factory.impl.FileTransferClientFactoryProducer;
import com.prats.filedownloader.entity.FileDownloadRequest;
import com.prats.filedownloader.entity.FileDownloadResponse;
import com.prats.filedownloader.properties.PropertyReader;

/* A callable class encapsulating the actual request and calling the respective file transfer client based on request protocol */
public class ProcessFileDownloadRequest implements Callable<FileDownloadResponse> {
	private final FileDownloadRequest request;
	private final CountDownLatch countDownLatch;

	public ProcessFileDownloadRequest(final FileDownloadRequest request,
			final CountDownLatch countDownLatch) {
		this.request = request;
		this.countDownLatch = countDownLatch;
	}

	public FileDownloadResponse call() {
		try {
			return getAndProcessResponse(this.request);
		} finally {
			countDownLatch.countDown();
		}
	}

	private FileDownloadResponse getAndProcessResponse(final FileDownloadRequest request) {
		FileTransferAbstractClientFactory factory = FileTransferClientFactoryProducer.getFactory();
		FileTransferClient fileTransferClient = factory
				.getFileTransferClient(request.getProtocol());

		Integer retryCount = getRetryCount();

		while (retryCount > 0) {
			long start = System.currentTimeMillis();
			boolean response = fileTransferClient.downloadFile(request);
			System.out.println("Time taken to download in sec : "
					+ (System.currentTimeMillis() - start) * 0.001 + " for request : " + request);
			if (response) {
				return (new FileDownloadResponse(request, response));
			}
			retryCount--;
		}
		return (new FileDownloadResponse(request, false));
	}

	private Integer getRetryCount() {
		Integer retryCount = Integer.valueOf(PropertyReader
				.getPropertyValue(PropertyReader.FILE_DOWNLOAD_RETRY_COUNT_PROPERTY));

		if (retryCount == null || retryCount <= 0) {
			// Minimum 1 try
			retryCount = 1;
		}
		return retryCount;
	}
}