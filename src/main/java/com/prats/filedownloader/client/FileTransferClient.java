package com.prats.filedownloader.client;

import com.prats.filedownloader.entity.FileDownloadRequest;

/* File transfer client interface which will enforce its implementors to 
 implement download file method based on specific protocol*/
public interface FileTransferClient {

	public boolean downloadFile(FileDownloadRequest fileDownloadRequest);
}
