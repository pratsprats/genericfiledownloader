package com.prats.filedownloader.client.factory;

import com.prats.filedownloader.client.FileTransferClient;
import com.prats.filedownloader.util.FileTransferProtocol;

/* Abstract factory interface to create transfer client based on protocol name*/
public interface FileTransferAbstractClientFactory {

	public FileTransferClient getFileTransferClient(FileTransferProtocol protocol);

}
