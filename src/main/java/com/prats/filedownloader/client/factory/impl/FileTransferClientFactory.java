package com.prats.filedownloader.client.factory.impl;

import com.prats.filedownloader.client.FileTransferClient;
import com.prats.filedownloader.client.factory.FileTransferAbstractClientFactory;
import com.prats.filedownloader.client.impl.FtpFileTransferClient;
import com.prats.filedownloader.client.impl.HttpFileTransferClient;
import com.prats.filedownloader.exception.ProtocolNotSupportedException;
import com.prats.filedownloader.util.FileTransferProtocol;

/* Abstract factory implementation to create transfer client based on protocol name. 
 This class makes sure that all the client implementations are created from single place*/
public class FileTransferClientFactory implements FileTransferAbstractClientFactory {

	public FileTransferClient getFileTransferClient(FileTransferProtocol protocol) {
		if (protocol == null) {
			throw new ProtocolNotSupportedException("Protocol is mandatory for download request");
		} else if (protocol.equals(FileTransferProtocol.HTTP)) {
			return new HttpFileTransferClient(false);
		} else if (protocol.equals(FileTransferProtocol.HTTPS)) {
			return new HttpFileTransferClient(true);
		} else if (protocol.equals(FileTransferProtocol.FTP)) {
			return new FtpFileTransferClient(false);
		} else if (protocol.equals(FileTransferProtocol.FTPS)) {
			return new FtpFileTransferClient(true);
		} else {
			throw new ProtocolNotSupportedException(
					"This protocol is not supported for download request : " + protocol);
		}
	}
}
