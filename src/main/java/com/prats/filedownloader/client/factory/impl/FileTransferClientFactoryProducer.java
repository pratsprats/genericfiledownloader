package com.prats.filedownloader.client.factory.impl;

import com.prats.filedownloader.client.factory.FileTransferAbstractClientFactory;

/* Abstract factory client producer */
public class FileTransferClientFactoryProducer {

	public static FileTransferAbstractClientFactory getFactory() {
		return new FileTransferClientFactory();
	}
}
