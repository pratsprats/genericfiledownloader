package com.prats.filedownloader.client.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;

import com.prats.filedownloader.client.FileTransferClient;
import com.prats.filedownloader.entity.FileDownloadRequest;
import com.prats.filedownloader.util.FileUtility;

/* Specific file transfer client implementation for FTP and FTPS protocol */
public class FtpFileTransferClient implements FileTransferClient {

	private boolean isSecureTransfer;

	public FtpFileTransferClient() {
		this(false);
	}

	public FtpFileTransferClient(boolean isSecureTransfer) {
		this.isSecureTransfer = isSecureTransfer;
	}

	public boolean downloadFile(FileDownloadRequest fileDownloadRequest) {
		FTPClient ftp = null;
		try {
			checkAndCreateDir(fileDownloadRequest);
			ftp = getFtpClient();
			boolean isSuccess = connectToFtpServer(fileDownloadRequest, ftp);
			if (isSuccess) {
				if (!ftp.login(fileDownloadRequest.getUserName(), fileDownloadRequest.getPassword())) {
					ftp.logout();
					System.err.println("Exception in connecting to FTP Server");
					return false;
				}
				ftp.setFileType(FTP.BINARY_FILE_TYPE);
				// Use passive mode as default because most of us are
				// behind firewalls these days.
				ftp.enterLocalPassiveMode();

				return transferFile(ftp, fileDownloadRequest);
			} else {
				return isSuccess;
			}

		} catch (Exception e) {
			// catch all exceptions for now and return false
			e.printStackTrace();
			return false;
		} finally {
			if (ftp != null) {
				try {
					ftp.logout();
				} catch (IOException e) {
					// do nothing
				}
			}
			if (ftp != null && ftp.isConnected()) {
				try {
					ftp.disconnect();
				} catch (IOException e) {
					// do nothing
				}
			}
		}
	}

	public void checkAndCreateDir(FileDownloadRequest genericFileDownloadRequest) {
		String localTempFile = genericFileDownloadRequest.getDestinationTempFile();
		String localFile = genericFileDownloadRequest.getDestinationFile();

		FileUtility.checkAndCreateDir(localTempFile);
		FileUtility.checkAndCreateDir(localFile);
	}

	private FTPClient getFtpClient() {
		FTPClient ftp = null;
		if (isSecureTransfer) {
			ftp = new FTPSClient("SSL");
		} else {
			ftp = new FTPClient();
		}
		return ftp;
	}

	private boolean connectToFtpServer(FileDownloadRequest fileDownloadRequest, FTPClient ftp) {
		try {
			ftp.connect(fileDownloadRequest.getHost(), fileDownloadRequest.getPort());
			int reply = ftp.getReplyCode();
			if (!FTPReply.isPositiveCompletion(reply)) {
				ftp.disconnect();
				System.err.println("FTP server refused connection.");
				return false;
			}
			return true;
		} catch (IOException e) {
			if (ftp.isConnected()) {
				try {
					ftp.disconnect();
				} catch (IOException f) {
					// do nothing
				}
			}
			System.err.println("Could not connect to server.");
			return false;
		}
	}

	private boolean transferFile(FTPClient ftp, FileDownloadRequest fileDownloadRequest)
			throws IOException {
		long originalFileSize = getFtpFileSize(ftp, fileDownloadRequest.getSourceFile());

		if (originalFileSize == getFileSize(fileDownloadRequest.getDestinationFile())) {
			System.out.println("File already downloaded : "
					+ fileDownloadRequest.getDestinationFile());
			return true;
		} else if (originalFileSize == getFileSize(fileDownloadRequest.getDestinationTempFile())) {
			System.out
					.println("File already downloaded to temp folder. Moving to destination folder.");
			FileUtility.moveFile(fileDownloadRequest.getDestinationTempFile(),
					fileDownloadRequest.getDestinationFile());
			return true;
		} else {
			return downloadFTPFile(ftp, fileDownloadRequest);
		}
	}

	// Download the FTP File from the FTP Server
	private boolean downloadFTPFile(FTPClient ftp, FileDownloadRequest request) throws IOException {
		InputStream in = retrieveFileStream(ftp, request.getSourceFile(),
				request.getDestinationTempFile());

		if (in != null) {
			boolean isSuccess = downloadFileFromStream(request, in);
			// move file from temp folder to destination folder atomically
			if (isSuccess) {
				FileUtility
						.moveFile(request.getDestinationTempFile(), request.getDestinationFile());
				return true;
			}
			return false;
		} else {
			return false;
		}
	}

	private InputStream retrieveFileStream(FTPClient ftp, String source, String destinationTemp)
			throws IOException {
		File localFile = new File(destinationTemp);
		if (localFile.exists()) {
			ftp.setRestartOffset(localFile.length());
			System.out.println("Resuming download from : " + localFile.length());
		}

		InputStream is = ftp.retrieveFileStream(source);
		int reply = ftp.getReplyCode();
		if (is == null
				|| (!FTPReply.isPositivePreliminary(reply) && !FTPReply.isPositiveCompletion(reply))) {
			System.out.println("Unable to retrieve file stream from : " + source);
			return null;
		}
		return is;
	}

	private boolean downloadFileFromStream(FileDownloadRequest request, InputStream in) {
		String destinationTemp = request.getDestinationTempFile();
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(destinationTemp, true);
			byte[] buf = new byte[1024];
			int bytesread = 0, bytesBuffered = 0;
			while ((bytesread = in.read(buf)) > -1) {
				fos.write(buf, 0, bytesread);
				bytesBuffered += bytesread;
				if (bytesBuffered > 1024 * 1024) { // flush after 1MB
					bytesBuffered = 0;
					// System.out.println("Flushing now");
					fos.flush();
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			if (fos != null) {
				try {
					fos.flush();
					fos.close();
				} catch (IOException e) {
					// do nothing
				}
			}
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					// do nothing
				}
			}
		}
	}

	private long getFtpFileSize(FTPClient ftp, String filePath) throws IOException {
		long fileSize = -1;
		FTPFile[] files = ftp.listFiles(filePath);
		if (files.length == 1 && files[0].isFile()) {
			fileSize = files[0].getSize();
		}
		return fileSize;
	}

	private long getFileSize(String file) {
		File f = new File(file);
		return f.length();
	}

	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		FileDownloadRequest fileDownloadRequest = new FileDownloadRequest();
		fileDownloadRequest.setHost("ftp.otenet.gr");
		fileDownloadRequest.setPort(21);
		fileDownloadRequest.setUserName("speedtest");
		fileDownloadRequest.setPassword("speedtest");
		fileDownloadRequest.setSourceFile("test10Mb.db");
		fileDownloadRequest.setDestinationFile("/Users/psachdeva1/Documents/test10Mb.db");
		fileDownloadRequest.setDestinationTempFile("/Users/psachdeva1/Documents/books/test10Mb.db");
		FtpFileTransferClient transferClient = new FtpFileTransferClient(false);
		System.out.println(transferClient.downloadFile(fileDownloadRequest));
		System.out.println("Time : " + (System.currentTimeMillis() - start) * 0.001);
	}

}
