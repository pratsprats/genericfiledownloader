package com.prats.filedownloader.client.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.security.Security;

import com.prats.filedownloader.client.FileTransferClient;
import com.prats.filedownloader.entity.FileDownloadRequest;
import com.prats.filedownloader.util.FileUtility;

/* Specific file transfer client implementation for HTTP and HTTPS protocol */
public class HttpFileTransferClient implements FileTransferClient {

	private boolean isSecureTransfer;

	public HttpFileTransferClient() {
		this(false);
	}

	public HttpFileTransferClient(boolean isSecureTransfer) {
		this.isSecureTransfer = isSecureTransfer;
	}

	public boolean downloadFile(FileDownloadRequest genericFileDownloadRequest) {
		setSSLProvider();
		return transferData(genericFileDownloadRequest);
	}

	private boolean transferData(FileDownloadRequest genericFileDownloadRequest) {
		String url = genericFileDownloadRequest.getSourceFile();
		String localTempFile = genericFileDownloadRequest.getDestinationTempFile();
		String localFile = genericFileDownloadRequest.getDestinationFile();

		boolean isFileTransferred = false;
		ReadableByteChannel readableByteChannel = null;
		FileOutputStream fileIOutputStream = null;

		try {
			checkAndCreateDir(genericFileDownloadRequest);

			long transferedSize = getFileSize(localFile);
			long sourceFileSize = getSourceFileSize(url);
			long transferedTempFileSize = getFileSize(localTempFile);

			if (sourceFileSize == transferedSize) {
				System.out.println("File already downloaded : " + localFile);
				isFileTransferred = true;
			} else if (sourceFileSize == transferedTempFileSize) {
				System.out.println("File already downloaded to temp folder. Moving to destination");
				FileUtility.moveFile(localTempFile, localFile);
				isFileTransferred = true;
			} else {
				URL website = new URL(url);
				URLConnection connection = website.openConnection();
				connection.setRequestProperty("Range", "bytes=" + transferedTempFileSize + "-");

				readableByteChannel = Channels.newChannel(connection.getInputStream());

				long remainingSize = connection.getContentLength();
				long buffer = remainingSize;

				if (remainingSize > 65536) {
					buffer = 1 << 16;
				}
				// System.out.println("Remaining size: " + remainingSize);

				fileIOutputStream = new FileOutputStream(localTempFile, true);

				System.out.println("Continue downloading at " + transferedTempFileSize);
				while (transferedTempFileSize != sourceFileSize) {
					long delta = fileIOutputStream.getChannel().transferFrom(readableByteChannel,
							transferedTempFileSize, buffer);
					transferedTempFileSize += delta;
					if (delta == 0) {
						break;
					}
				}

				// move file from temp folder to destination folder atomically
				if (transferedTempFileSize == sourceFileSize) {
					FileUtility.moveFile(localTempFile, localFile);
					isFileTransferred = true;
				}

			}
		} catch (Exception e) {
			// catch all exceptions for now and return false
			isFileTransferred = false;
			e.printStackTrace();
		} finally {
			if (readableByteChannel != null) {
				try {
					readableByteChannel.close();
				} catch (IOException e) {
				}
			}

			if (fileIOutputStream != null) {
				try {
					fileIOutputStream.close();
				} catch (IOException e) {
				}
			}

		}
		return isFileTransferred;
	}

	private void checkAndCreateDir(FileDownloadRequest genericFileDownloadRequest) {
		String localTempFile = genericFileDownloadRequest.getDestinationTempFile();
		String localFile = genericFileDownloadRequest.getDestinationFile();

		FileUtility.checkAndCreateDir(localTempFile);
		FileUtility.checkAndCreateDir(localFile);
	}

	private long getSourceFileSize(String url) throws IOException {
		URL website = new URL(url);
		URLConnection connection = website.openConnection();
		connection.connect();
		return connection.getContentLength();
	}

	private long getFileSize(String file) {
		File f = new File(file);
		return f.length();
	}

	@SuppressWarnings("restriction")
	private void setSSLProvider() {
		if (isSecureTransfer) {
			System.setProperty("java.protocol.handler.pkgs",
					"com.sun.net.ssl.internal.www.protocol");
			Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
		}

	}

	public static void main(String[] args) {
		HttpFileTransferClient httpFileTransferClient = new HttpFileTransferClient(false);
		FileDownloadRequest request = new FileDownloadRequest();
		request.setSourceFile("http://speedtest.ftp.otenet.gr/files/test10Mb.db");
		request.setDestinationFile("/Users/psachdeva1/Documents/test10Mb.db");
		request.setDestinationTempFile("/Users/psachdeva1/Documents/books/test10Mb.db");

		FileDownloadRequest r = new FileDownloadRequest();
		r.setSourceFile("https://code.jquery.com/jquery-3.0.0.min.js");
		r.setDestinationFile("/Users/psachdeva1/Documents/test/me/abc.txt");
		r.setDestinationTempFile("/Users/psachdeva1/Documents/jquery-3.0.0.min.js");

		httpFileTransferClient.downloadFile(r);
	}

}
