package com.prats.filedownloader.entity;

/* A specific class to wrap file download request for ftp and ftps protocol which extends the generic request*/
public class FileDownloadRequest extends GenericFileDownloadRequest {

	private String host;
	private Integer port;
	private String userName;
	private String password;

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "FileDownloadRequest [host=" + host + ", port=" + port + ", userName=" + userName
				+ ", password=" + password + ", toString()=" + super.toString() + "]";
	}

}
