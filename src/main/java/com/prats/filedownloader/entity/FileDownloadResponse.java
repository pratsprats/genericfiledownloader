package com.prats.filedownloader.entity;

public class FileDownloadResponse {

	private final FileDownloadRequest request;
	private final Boolean response;

	public FileDownloadResponse(final FileDownloadRequest request, final Boolean response) {
		this.request = request;
		this.response = response;
	}

	public FileDownloadRequest getRequest() {
		return request;
	}

	public Boolean getResponse() {
		return response;
	}

	@Override
	public String toString() {
		return "FileDownloadResponse [request=" + request + ", response=" + response + "]";
	}

}
