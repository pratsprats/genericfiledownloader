package com.prats.filedownloader.entity;

import com.prats.filedownloader.util.FileTransferProtocol;

/* A generic class to wrap file download request */
public class GenericFileDownloadRequest {
	private String sourceFile;
	private String destinationTempFile;
	private String destinationFile;
	private FileTransferProtocol protocol;

	public String getDestinationTempFile() {
		return destinationTempFile;
	}

	public void setDestinationTempFile(String destinationTempFile) {
		this.destinationTempFile = destinationTempFile;
	}

	public String getSourceFile() {
		return sourceFile;
	}

	public void setSourceFile(String sourceFile) {
		this.sourceFile = sourceFile;
	}

	public String getDestinationFile() {
		return destinationFile;
	}

	public void setDestinationFile(String destinationFile) {
		this.destinationFile = destinationFile;
	}

	public FileTransferProtocol getProtocol() {
		return protocol;
	}

	public void setProtocol(FileTransferProtocol protocol) {
		this.protocol = protocol;
	}

	@Override
	public String toString() {
		return "GenericFileDownloadRequest [sourceFile=" + sourceFile + ", destinationTempFile="
				+ destinationTempFile + ", destinationFile=" + destinationFile + ", protocol="
				+ protocol + "]";
	}

}
