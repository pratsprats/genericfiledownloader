package com.prats.filedownloader.entity;

import java.util.concurrent.Future;

public class SubmittedFileDownloadJob {

	final Future<FileDownloadResponse> future;

	public SubmittedFileDownloadJob(final Future<FileDownloadResponse> job) {
		this.future = job;
	}

	@Override
	public String toString() {
		return "SubmittedFileDownloadJob [future=" + future + "]";
	}

	public Future<FileDownloadResponse> getFuture() {
		return future;
	}
}
