package com.prats.filedownloader.exception;

public class ProtocolNotSupportedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5569365249068569986L;

	public ProtocolNotSupportedException() {
	}

	public ProtocolNotSupportedException(String str) {
		super(str);
	}

	public ProtocolNotSupportedException(Throwable cause) {
		super(cause);
	}

	public ProtocolNotSupportedException(String message, Throwable cause) {
		super(message, cause);
	}

	public ProtocolNotSupportedException(StringBuilder message, Throwable cause) {
		this(message.toString(), cause);
	}
}
