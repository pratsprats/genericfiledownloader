package com.prats.filedownloader.properties;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/* A generic property file reader class to load properties from config.properties. 
 Properties should be defined in key value pair */
public class PropertyReader {
	private static final String CONFIG_FILE_NAME = "config.properties";
	private static final Properties properties = new Properties();

	public static final String THREAD_POOL_SIZE_PROPERTY = "threadPoolSize";
	public static final String FILE_DOWNLOAD_RETRY_COUNT_PROPERTY = "fileDownloadRetryCount";
	public static final String DESTINATION_FOLDER_LOC_PROPERTY = "destinationFolderLocation";
	public static final String DESTINATION_TEMP_FOLDER_LOC_PROPERTY = "destinationTempFolderLocation";

	static {
		loadPropertyFile();
	}

	private static void loadPropertyFile() {
		InputStream input = null;

		try {
			input = PropertyReader.class.getClassLoader().getResourceAsStream(CONFIG_FILE_NAME);

			if (input == null) {
				throw new FileNotFoundException("Unable to find property file : "
						+ CONFIG_FILE_NAME);
			}

			properties.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					// do nothing
				}
			}
		}
	}

	public static String getPropertyValue(String propertyName) {
		return properties.getProperty(propertyName);
	}
}
