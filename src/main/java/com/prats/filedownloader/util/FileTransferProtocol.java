package com.prats.filedownloader.util;

/* Enum representing the supported protocols for file download */
public enum FileTransferProtocol {
	HTTP("http"), HTTPS("https"), FTP("ftp"), FTPS("ftps");

	private String name;

	private FileTransferProtocol(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public static FileTransferProtocol getFileTransferProtocol(String name) {
		if (name != null) {
			return name.equalsIgnoreCase(HTTP.getName()) ? HTTP : name.equalsIgnoreCase(HTTPS
					.getName()) ? HTTPS : name.equalsIgnoreCase(FTP.getName()) ? FTP : name
					.equalsIgnoreCase(FTPS.getName()) ? FTPS : null;
		} else {
			return null;
		}
	}

}
