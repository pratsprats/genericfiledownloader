package com.prats.filedownloader.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

/* File Utility to move file from temp location to destination location atomically */
public class FileUtility {

	public static void moveFile(String source, String destination) throws IOException {
		Files.move(new File(source).toPath(), new File(destination).toPath(),
				StandardCopyOption.ATOMIC_MOVE);
	}

	public static void checkAndCreateDir(String filePath) {
		File file = new File(filePath);
		String absolutePath = file.getAbsolutePath();
		String dirPath = absolutePath.substring(0, absolutePath.lastIndexOf(File.separator));
		File dir = new File(dirPath);

		if (!dir.exists()) {
			if (dir.mkdirs()) {
				System.out.println("New directories created");
			}
		}
	}

}
