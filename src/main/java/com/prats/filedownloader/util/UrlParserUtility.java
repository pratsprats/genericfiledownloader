package com.prats.filedownloader.util;

import java.net.MalformedURLException;
import java.net.URL;

import com.prats.filedownloader.entity.FileDownloadRequest;
import com.prats.filedownloader.properties.PropertyReader;

/* A Util class to parse the input string and create File Download request */
public class UrlParserUtility {

	private static final Integer DEFAULT_FTP_PORT = 21;
	private static final Integer DEFAULT_FTPS_PORT = 22;

	public static FileDownloadRequest getFileDownloadRequest(String urlString) {
		FileDownloadRequest request = null;
		try {
			if (urlString != null && !urlString.isEmpty()) {
				String tempLoc = PropertyReader
						.getPropertyValue(PropertyReader.DESTINATION_TEMP_FOLDER_LOC_PROPERTY);
				String destinationLoc = PropertyReader
						.getPropertyValue(PropertyReader.DESTINATION_FOLDER_LOC_PROPERTY);

				if (urlString.startsWith(FileTransferProtocol.HTTP.getName())
						|| urlString.startsWith(FileTransferProtocol.HTTPS.getName())) {
					URL url = new URL(urlString);

					request = new FileDownloadRequest();
					// setting full url as source
					request.setSourceFile(urlString);
					request.setProtocol(FileTransferProtocol.getFileTransferProtocol(url
							.getProtocol()));
					// appending protocol name and url path in the destination
					// location
					request.setDestinationFile(destinationLoc + url.getProtocol() + url.getPath());
					request.setDestinationTempFile(tempLoc + url.getProtocol() + url.getPath());
					request.setHost(url.getHost());
					request.setPort(url.getPort());

				} else if (urlString.startsWith(FileTransferProtocol.FTPS.getName())) {
					// java.net.URL does not support FTPS. Hack to parse FTPS
					// protocol using URL
					urlString = urlString.replace(FileTransferProtocol.FTPS.getName(),
							FileTransferProtocol.FTP.getName());
					URL url = new URL(urlString);

					request = new FileDownloadRequest();
					// setting path as source
					String path = url.getPath();
					if (path.startsWith("/")) {
						path = path.substring(1);
					}
					request.setSourceFile(path);
					// hard coding it to FTPS since the java.net.URL does not
					// support FTPS
					request.setProtocol(FileTransferProtocol.FTPS);
					request.setDestinationFile(destinationLoc + FileTransferProtocol.FTPS.getName()
							+ url.getPath());
					request.setDestinationTempFile(tempLoc + FileTransferProtocol.FTPS.getName()
							+ url.getPath());
					request.setHost(url.getHost());
					request.setPort(url.getPort() > 0 ? url.getPort() : DEFAULT_FTPS_PORT);

					String userInfo = url.getUserInfo();
					if (userInfo != null && !userInfo.isEmpty()) {
						String[] userData = userInfo.split(":");
						if (userData != null) {
							if (userData.length > 0) {
								String userName = userData[0];
								request.setUserName(userName);
							}
							if (userData.length > 1) {
								String password = userData[1];
								request.setPassword(password);
							}
						}
					}
				} else if (urlString.startsWith(FileTransferProtocol.FTP.getName())) {
					URL url = new URL(urlString);

					request = new FileDownloadRequest();
					// setting path as source
					String path = url.getPath();
					if (path.startsWith("/")) {
						path = path.substring(1);
					}
					request.setSourceFile(path);
					request.setProtocol(FileTransferProtocol.getFileTransferProtocol(url
							.getProtocol()));
					request.setDestinationFile(destinationLoc + url.getProtocol() + url.getPath());
					request.setDestinationTempFile(tempLoc + url.getProtocol() + url.getPath());
					request.setHost(url.getHost());
					request.setPort(url.getPort() > 0 ? url.getPort() : DEFAULT_FTP_PORT);

					String userInfo = url.getUserInfo();
					if (userInfo != null && !userInfo.isEmpty()) {
						String[] userData = userInfo.split(":");
						if (userData != null) {
							if (userData.length > 0) {
								String userName = userData[0];
								request.setUserName(userName);
							}
							if (userData.length > 1) {
								String password = userData[1];
								request.setPassword(password);
							}
						}
					}
				}
			}
		} catch (MalformedURLException e) {
			System.out.println("Url string is malformed : " + urlString);
		}

		return request;
	}

	public static void main(String[] args) {
		String s = "http://my.file.com/abc.txt";

		String s1 = "https://my.file.com/folder/abc.txt";

		String s2 = "ftp://speedtest:speedtest@ftp.otenet.gr/test1Mb.db";

		String s3 = "ftps://speedtest:speedtest@ftp.otenet.gr/tt/test1Mb.db";

		System.out.println(getFileDownloadRequest(s));
		System.out.println(getFileDownloadRequest(s1));
		System.out.println(getFileDownloadRequest(s2));
		System.out.println(getFileDownloadRequest(s3));
	}

}
