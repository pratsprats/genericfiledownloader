package com.prats.filedownloader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.prats.filedownloader.entity.FileDownloadResponse;
import com.prats.filedownloader.properties.PropertyReader;
import com.prats.filedownloader.util.FileTransferProtocol;

public class GenericFileDownloaderTest {

	@Test
	public void testDownloadFiles() {
		List<String> list = new ArrayList<String>();
		list.add("http://speedtest.ftp.otenet.gr/files/test1Mb.db");
		list.add("https://code.jquery.com/jquery-3.0.0.min.js");
		list.add("ftp://speedtest:speedtest@ftp.otenet.gr/test1Mb.db");
		GenericFileDownloader genericFileDownloaderMain = new GenericFileDownloader();
		List<FileDownloadResponse> results = genericFileDownloaderMain.downloadFiles(list);

		Assert.assertNotNull(results);
		Assert.assertEquals(results.size(), list.size());

		for (FileDownloadResponse f : results) {
			Assert.assertEquals(f.getResponse(), Boolean.TRUE);
		}

		String tempLoc = PropertyReader
				.getPropertyValue(PropertyReader.DESTINATION_TEMP_FOLDER_LOC_PROPERTY);
		String destinationLoc = PropertyReader
				.getPropertyValue(PropertyReader.DESTINATION_FOLDER_LOC_PROPERTY);
		cleanup(tempLoc, destinationLoc);
	}

	@Test
	public void testErrorDownloadFiles() {
		List<String> list = new ArrayList<String>();
		list.add("http://speedtest.ftp.otenet.gr/files/test1Mb.db");
		list.add("https://code.jquery.com/jquery-3.0.0.min.js");
		// incorrect password
		list.add("ftp://speedtest:speedtes@ftp.otenet.gr/test1Mb.db");
		GenericFileDownloader genericFileDownloaderMain = new GenericFileDownloader();
		List<FileDownloadResponse> results = genericFileDownloaderMain.downloadFiles(list);

		Assert.assertNotNull(results);
		Assert.assertEquals(results.size(), list.size());

		for (FileDownloadResponse f : results) {
			if (f.getRequest().getProtocol().equals(FileTransferProtocol.FTP)) {
				Assert.assertEquals(f.getResponse(), Boolean.FALSE);
			} else {
				Assert.assertEquals(f.getResponse(), Boolean.TRUE);
			}
		}

		String tempLoc = PropertyReader
				.getPropertyValue(PropertyReader.DESTINATION_TEMP_FOLDER_LOC_PROPERTY);
		String destinationLoc = PropertyReader
				.getPropertyValue(PropertyReader.DESTINATION_FOLDER_LOC_PROPERTY);
		cleanup(tempLoc, destinationLoc);
	}

	private void cleanup(String destinationTempFile, String destinationFile) {
		File dest = new File(destinationTempFile);
		if (dest.exists()) {
			dest.delete();
		}
		dest = new File(destinationFile);
		if (dest.exists()) {
			dest.delete();
		}
	}

}
