package com.prats.filedownloader.client.impl;

import java.io.File;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.prats.filedownloader.entity.FileDownloadRequest;

public class FtpFileTransferClientTest {

	@Test
	public void testDownloadFile() {

		FileDownloadRequest fileDownloadRequest = new FileDownloadRequest();

		String destinationTempFile = "/Users/psachdeva1/Documents/prats/ftp/temp/test1Mb.db";
		String destinationFile = "/Users/psachdeva1/Documents/prats/ftp/test1Mb.db";

		fileDownloadRequest.setHost("ftp.otenet.gr");
		fileDownloadRequest.setPort(21);
		fileDownloadRequest.setUserName("speedtest");
		fileDownloadRequest.setPassword("speedtest");
		fileDownloadRequest.setSourceFile("test1Mb.db");
		fileDownloadRequest.setDestinationFile(destinationFile);
		fileDownloadRequest.setDestinationTempFile(destinationTempFile);

		FtpFileTransferClient transferClient = new FtpFileTransferClient();
		boolean success = transferClient.downloadFile(fileDownloadRequest);

		Assert.assertEquals(success, true);

		File file = new File(destinationTempFile);
		Assert.assertEquals(file.exists(), false);

		File file1 = new File(destinationFile);
		Assert.assertEquals(file1.exists(), true);

		cleanup(destinationTempFile, destinationFile);
	}

	@Test
	public void testErrorDownloadFile() {

		FileDownloadRequest fileDownloadRequest = new FileDownloadRequest();

		String destinationTempFile = "/Users/psachdeva1/Documents/prats/ftp/temp/test1Mb.db";
		String destinationFile = "/Users/psachdeva1/Documents/prats/ftp/test1Mb.db";

		fileDownloadRequest.setHost("ftp.otenet.gr");
		fileDownloadRequest.setPort(21);
		
		// incorrect user name
		fileDownloadRequest.setUserName("speetest");
		fileDownloadRequest.setPassword("speedtest");
		fileDownloadRequest.setSourceFile("test1Mb.db");
		fileDownloadRequest.setDestinationFile(destinationFile);
		fileDownloadRequest.setDestinationTempFile(destinationTempFile);

		FtpFileTransferClient transferClient = new FtpFileTransferClient();
		boolean success = transferClient.downloadFile(fileDownloadRequest);

		Assert.assertEquals(success, false);

		File file = new File(destinationTempFile);
		Assert.assertEquals(file.exists(), false);

		File file1 = new File(destinationFile);
		Assert.assertEquals(file1.exists(), false);

		cleanup(destinationTempFile, destinationFile);
	}
	
	private void cleanup(String destinationTempFile, String destinationFile) {
		File dest = new File(destinationTempFile);
		if (dest.exists()) {
			dest.delete();
		}
		dest = new File(destinationFile);
		if (dest.exists()) {
			dest.delete();
		}
	}

}
