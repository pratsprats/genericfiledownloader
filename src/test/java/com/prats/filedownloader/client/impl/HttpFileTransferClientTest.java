package com.prats.filedownloader.client.impl;

import java.io.File;
import java.net.URL;
import java.net.URLConnection;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.prats.filedownloader.entity.FileDownloadRequest;

public class HttpFileTransferClientTest {

	@Test
	public void testDownloadFile() {
		HttpFileTransferClient httpFileTransferClient = new HttpFileTransferClient();

		FileDownloadRequest r = new FileDownloadRequest();
		String sourceUrl = "https://code.jquery.com/jquery-3.0.0.min.js";
		String destinationFile = "/Users/psachdeva1/Documents/prats/https/jquery-3.0.0.min.js";
		String destinationTempFile = "/Users/psachdeva1/Documents/prats/https/temp/jquery-3.0.0.min.js";

		r.setSourceFile(sourceUrl);
		r.setDestinationFile(destinationFile);
		r.setDestinationTempFile(destinationTempFile);

		boolean result = httpFileTransferClient.downloadFile(r);

		Assert.assertEquals(result, true);

		File file = new File(destinationTempFile);
		Assert.assertEquals(file.exists(), false);

		File file1 = new File(destinationFile);
		Assert.assertEquals(file1.exists(), true);

		long actualSize = getSourceFileSize(sourceUrl);
		Assert.assertEquals(actualSize, file1.length());

		cleanup(destinationTempFile, destinationFile);
	}

	@Test
	public void testErrorDownloadFile() {
		HttpFileTransferClient httpFileTransferClient = new HttpFileTransferClient();

		FileDownloadRequest r = new FileDownloadRequest();

		// Unknown host url
		String sourceUrl = "https://cde.jquery.com/jquery-3.0.0.min.js";
		String destinationFile = "/Users/psachdeva1/Documents/prats/https/jquery-3.0.0.min.js";
		String destinationTempFile = "/Users/psachdeva1/Documents/prats/https/temp/jquery-3.0.0.min.js";

		r.setSourceFile(sourceUrl);
		r.setDestinationFile(destinationFile);
		r.setDestinationTempFile(destinationTempFile);

		boolean result = httpFileTransferClient.downloadFile(r);

		Assert.assertEquals(result, false);

		File file = new File(destinationTempFile);
		Assert.assertEquals(file.exists(), false);

		File file1 = new File(destinationFile);
		Assert.assertEquals(file1.exists(), false);

		cleanup(destinationTempFile, destinationFile);
	}

	private long getSourceFileSize(String url) {
		try {
			URL website = new URL(url);
			URLConnection connection = website.openConnection();
			connection.connect();
			return connection.getContentLength();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	private void cleanup(String destinationTempFile, String destinationFile) {
		File dest = new File(destinationTempFile);
		if (dest.exists()) {
			dest.delete();
		}
		dest = new File(destinationFile);
		if (dest.exists()) {
			dest.delete();
		}
	}
}
