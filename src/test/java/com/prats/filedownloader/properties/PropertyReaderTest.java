package com.prats.filedownloader.properties;

import org.testng.Assert;
import org.testng.annotations.Test;

public class PropertyReaderTest {

	@Test
	public void testPropertyReader() {
		String destinationLoc = PropertyReader
				.getPropertyValue(PropertyReader.DESTINATION_FOLDER_LOC_PROPERTY);
		Assert.assertEquals(destinationLoc, "/Users/psachdeva1/Documents/downloadedFiles/");

		String destinationTempLoc = PropertyReader
				.getPropertyValue(PropertyReader.DESTINATION_TEMP_FOLDER_LOC_PROPERTY);
		Assert.assertEquals(destinationTempLoc, "/Users/psachdeva1/Documents/tempFiles/");

		Integer retryCount = Integer.valueOf(PropertyReader
				.getPropertyValue(PropertyReader.FILE_DOWNLOAD_RETRY_COUNT_PROPERTY));
		Assert.assertEquals(retryCount, Integer.valueOf(2));

		Integer threadPool = Integer.valueOf(PropertyReader
				.getPropertyValue(PropertyReader.THREAD_POOL_SIZE_PROPERTY));
		Assert.assertEquals(threadPool, Integer.valueOf(10));

	}

}
